import React from 'react';
import fbImg from './assets/facebook.svg';
import twitterImg from './assets/twitter.svg';
import whatsappImg from './assets/whatsapp.svg';
import instaImg from './assets/instagram.svg';
 
import './styles/socialMedia.scss'

const SocialMedia = () => {
    return (
        <div className="container">
            <div className="fbContainer socialContainers">
                <img className="fbImg" src={fbImg} alt="facebook" />
            </div>

            <div className="twitterContainer socialContainers">
                <img className="twitterImg" src={twitterImg} alt="twitter" />
            </div>

            <div className="whatsappContainer socialContainers">
                <img className="whatsappImg" src={whatsappImg} alt="whatsapp" />
            </div>

            <div className="instaContainer socialContainers">
                <img className="instaImg" src={instaImg} alt="instagram" />
            </div>
        </div>
    );
}

export default SocialMedia;