import React, { useState, useEffect } from 'react';
import './styles/sales.scss';
import './styles/button.scss';
import SocialMedia from './SocialMedia';
import Button from './Button';
import CardStack from './CardStack';

const RightBackground = () => {
    // const [card1, setcard1] = useState('card1');
    // const [card2, setcard2] = useState('card2');
    // const [card3, setcard3] = useState('card3');
    // const [card4, setcard4] = useState('card4');
    // const [card5, setcard5] = useState('card5');

    const [{ card1, card2, card3, card4, card5, card6 }, setcard] = useState({ card1: 'card1', card2: 'card2', card3: 'card3', card4: 'card4', card5: 'card5', card6: 'card6' });
    // const [{ card1, card2, card3, card4, card5 }, setcard] = useState({ card1: 'card1', card2: 'card2', card3: 'card3', card4: 'card4', card5: 'card5' });
    // const [temp, setTemp] = useState('');

    // const [cardMsgA, setCardMsgA] = useState("I'm card1");
    // const [cardMsgB, setCardMsgB] = useState("I'm card2");
    // const [cardMsgC, setCardMsgC] = useState("I'm card3");
    // const [cardMsgD, setCardMsgD] = useState("I'm card4");
    // const [cardMsgE, setCardMsgE] = useState("I'm card5");

    const [{ cardMsg1, cardMsg2, cardMsg3, cardMsg4, cardMsg5 }, setcardMsg] = useState({ cardMsg1: "I'm card 1", cardMsg2: "I'm card 2", cardMsg3: "I'm card 3", cardMsg4: "I'm card 4", cardMsg5: "I'm card 5" });

    // let cardArray = ["card1", "card2", "card3", "card4", "card5", "card6"];
    // let cardArray = ["card1", "card2", "card3", "card4", "card5"];
    let cardArray = ["card6", "card5", "card4", "card3", "card2", "card1"];
    let cardMsgArray = ["I'm card 5", "I'm card 4", "I'm card 3", "I'm card 2", "I'm card 1"]; 
    // const cardArray = [card1, card2, card3, card4, card5];
    let temp1 = "";
    let temp2 = "";

    useEffect(() => {

        // setTimeout(() => {
        //   // this.setState ({
        //   //   sHeading1: "100%"
        //   // });
        // // console.log("effect");
        // //   setHeading1("79%");
        //   // setCaret1("transparent");
        //   // setCaret1("blink-caret-transparent step-end infinite");
        // }, 2000);

        // setTimeout(() => {
        //   // this.setState ({
        //   //   sHeading1: "100%"
        //   // });
        // // console.log("effect");
        // //   setHeading2("100%");
        //   // setCaret2("transparent");

        // }, 4000);

        // setTimeout(() => {
        //   // this.setState ({
        //   //   sHeading1: "100%"
        //   // });
        // // console.log("effect");
        // //   setHeading3("100%");
        //   // setCaret3("transparent");

        // }, 6000);

        //  if(cardA === "card1"){
        //     setcardA("card2");
        //     console.log("card A")
        //     setCardMsgA("I'm card 1");
        //  }

        //  if(cardB === "card2"){
        //      setcardB("card3");
        //      console.log("card B")
        //      setCardMsgB("I'm card 2");
        //   }

        //   if(cardC === "card3"){
        //      setcardC("card4");
        //      console.log("card C");
        //      setCardMsgC("I'm card 3");
        //   }

        //   if(cardD === "card4"){
        //      setcardD("card5");
        //      console.log("card D")
        //      setCardMsgD("I'm card 4");
        //   }

        //   if(cardE === "card5"){
        //      setcardE("card1");
        //      console.log("card E")
        //      setCardMsgE("I'm card 5");
        //   }

        setInterval(() => {
            for (let i = 0; i < cardArray.length - 1; i++) {
                temp1 = cardArray[i];
                temp2 = cardMsgArray[i];
                cardArray[i] = cardArray[i + 1];
                cardMsgArray[i] = cardMsgArray[i+1];
                cardArray[i + 1] = temp1;
                cardMsgArray[i+1] = temp2;
            }

            // for (let j = 0; j < cardArray.length - 1; j++) {
            //     setcard1(`card${j}`);
            // }

            setcard({ card1: cardArray[0], card2: cardArray[1], card3: cardArray[2], card4: cardArray[3], card5: cardArray[4], card6: cardArray[5] });
            // setcard({ card1: cardArray[0], card2: cardArray[1], card3: cardArray[2], card4: cardArray[3], card5: cardArray[4] });
            // setcard1(cardArray[0]);
            // setcard2(cardArray[1]);
            // setcard3(cardArray[2]);
            // setcard4(cardArray[3]);
            // setcard5(cardArray[4]);
            setcardMsg({cardMsg1: cardMsgArray[0], cardMsg2: cardMsgArray[1], cardMsg3: cardMsgArray[2], cardMsg4: cardMsgArray[3], cardMsg5: cardMsgArray[4]});
            // for(let j=0; j<5; j++){
            //     setcard({ [`card${j+1}`]: cardArray[j] });
            // }

            console.log(cardArray);
            console.log(cardMsgArray);
        }, 5000);


        // setInterval(() => {
        //     for (let i = 1; i < 5; i++) {
        //         let functionName = "setcard1";
        //         let cards = eval('card' + (i));
        //         // let cards = 'card'[i];

        //         setTemp(cards);
        //         setcard({ [eval('card' + (i))]: eval('card' + (i + 1)) });
        //         setcard({ [eval('card' + (i + 1))]: temp });
        //         // cardArray[i] = cardArray[i + 1];
        //         // cardArray[i + 1] = temp;
        //         console.log(temp);
        //     }
        //     // console.log(eval('card' + i));
        // }, 5000);

        // setInterval(() => {
        //     for (let i = 1; i < 4; i++) {
        //         temp = ['card' + i];
        //         window['card' + i] = window['card' + (i + 1)];
        //         window['card' + (i + 1)] = temp;
        //     }
        //     console.log(temp);
        // }, 5000);
    }, []);
    console.log(card1);
    return (
        <div className="rightBackground" >
            <CardStack customClass={card1} customMsg={card1} />
            <CardStack customClass={card2} customMsg={card2} />
            <CardStack customClass={card3} customMsg={card3} />
            <CardStack customClass={card4} customMsg={card4} />
            <CardStack customClass={card5} customMsg={card5} /> 
            <SocialMedia />
            <Button />
        </div>
    );
}

export default RightBackground;