import React from 'react';
import mobile from './assets/01-Mockup-Galaxy-S10-Front.png'
import './styles/image.scss'

const Image = () => {
    // const galaxyPhone={
    //     width: "800px",
    //     marginLeft: "5px",
    //     backgroundColor: "red",
    //     textAlign: "right" 
    // }

    return (
        <div className="imageContainer">
            <img className="galaxyPhone" src={mobile} alt="galaxy phone"/>
        </div>
    );
}

export default Image;