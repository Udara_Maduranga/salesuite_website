import React, { useState, useEffect } from 'react';
import './styles/message.scss';

const Message = (state) => {
  // const initialMsg = {
  //   color: "#4D0A11",
  //   fontSize: "45px",
  //   fontFamily: "Montserrat"
  // }

  // const afterMsg = {
  //   color: "black",
  //   fontSize: "45px",
  //   fontFamily: "Montserrat"
  // }

  // setTimeout(() => {
  //   document.getElementsByClassName("heading1").style.width = "100%"; 
  // }, 2000);

  const [sHeading1, setHeading1] = useState("0%");
  const [sHeading2, setHeading2] = useState("0%");
  const [sHeading3, setHeading3] = useState("0%");

  // const [sCaret1, setCaret1] = useState("orange");
  // const [sCaret2, setCaret2] = useState("orange");
  // const [sCaret3, setCaret3] = useState("orange");

  useEffect(() => {
    setTimeout(() => {
      // this.setState ({
      //   sHeading1: "100%"
      // });
    // console.log("effect");
      setHeading1("79%");
      // setCaret1("transparent");
      // setCaret1("blink-caret-transparent step-end infinite");
    }, 2000);

    setTimeout(() => {
      // this.setState ({
      //   sHeading1: "100%"
      // });
    // console.log("effect");
      setHeading2("100%");
      // setCaret2("transparent");

    }, 4000);

    setTimeout(() => {
      // this.setState ({
      //   sHeading1: "100%"
      // });
    // console.log("effect");
      setHeading3("100%");
      // setCaret3("transparent");

    }, 6000);
  },[]);

  return (
    <div className="message">
      {/* <p className="initialMsg msg">The best</p>
      <p className="afterMsg msg"> app to streamline your selling process</p> */}

      {/* <p><div className="initialMsg msg">The best</div> app<br/>
        <div className="afterMsg msg">to streamline your <br/> selling process</div> </p>  */}
      {/* 
       <p><span className="initialMsg msg">The best</span>
       app to streamline your selling process </p> */}

      <p className="heading1" style={{ width: sHeading1}}><span className="initialMsg msg">The best</span> app</p>
      <p className="heading2" style={{ width: sHeading2}}>to streamline your</p>
      <p className="heading3" style={{ width: sHeading3}}>selling process</p>

      {/* <p className="heading1" ><span className="initialMsg msg">The best</span> app</p>
      <p className="heading2">to streamline your</p>
      <p className="heading3">selling process</p> */}
    </div>
  );
}

export default Message;