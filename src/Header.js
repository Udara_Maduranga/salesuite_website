import React from 'react';
import image from './assets/logoArtboard.png'
import './styles/header.scss'

const Header = () => {
    // const salesImage = {
    //     height: "60px",
    //     marginLeft: "30px",
    //     marginTop: "40px",
    // }

    return (
        <div>
            <img className="salesImage" src={image} alt="sales suite" />
        </div>
    );
}

export default Header;
