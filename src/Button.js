import React from 'react';

const Button = () => {
    return (
        <div className="btnContainer" >
            <button className="signupBtn">Sign Up Now!</button>
        </div>
    );
}

export default Button;