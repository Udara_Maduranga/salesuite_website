import React from 'react';
//import './App.css';
import LeftBackground from './LeftBackground';
import RightBackground from './RightBackground';
import './styles/sales.scss';

const App = () => {
  
  return (
    <div className="app">
      <LeftBackground />
      <RightBackground />
    </div>
  );
}

export default App;
