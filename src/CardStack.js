import React from 'react';
import './styles/cardStack.scss'

const CardStack = (props) => {
    // const {stack1, stack2} = this.props;

    return (
        <div className= "cardContainer">
            <div className= {`card ${props.customClass}`}>
                <div className="card-body">
                    {/* aa */}
                    {props.customMsg}
                </div>
            </div>
        </div>
    );
}

export default CardStack;
