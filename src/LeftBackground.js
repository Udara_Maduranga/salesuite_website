import React from 'react';
import Header from './Header';
import Message from './Message';
import Image from './Image';
import './styles/sales.scss';

const LeftBackground = () => {
    return (
        <div className="leftBackground">
            <Header />
            <Message />
            <Image />
        </div>
    );
}

export default LeftBackground;